![Webparts](https://i.ibb.co/sRpwQD2/logo-webparts-branco.png)
========  
Teste para avaliar as habilidades dos candidatos a Desenvolvedor PHP.

# Introdução
# Este teste tem como objetivo avaliar seu conhecimento como desenvolvedor PHP. O teste de avaliação avaliará suas habilidades em:
- Seu nível de conhecimento da linguagem PHP OOP (PHP versão 7 +)
- Conhecimento dos principais padrões usados pela pela linguagem
- Padrões de código e boas práticas de programação
- Princípios SOLID e recomendações de padrões PHP (PSR)
- Composer
- Docker (diferencial)

# O que será avaliado?

 - Estrutura e organização de código e arquivos
 - Soluções adotadas
 - Tecnologias utilizadas
 - Qualidade
 - PSR Patterns, Design Patterns

# Como publicar o seu teste?

 - Criar um repositório privado no Bitbucket 
 - Criar uma instalação básica do Laravel e publicar, caso tenha escolhido usar o Docker publicar também toda a configuração (docker-compose YAML)
 - Quando terminar, convidar o usuário **adrianowebparts** para acessar o repositório e avaliar o teste

# Instruções

O foco principal do nosso teste de avaliação é avaliar suas habilidades e conhecimentos como desenvolvedor PHP.

Você tem total liberdade para aumentar alguns conhecimentos que achar interessantes para aplicar na avaliação.

**Não há problema se você não conseguir executar todos os recursos da avaliação, faça o máximo que puder.**

Criem uma aplicação utilizando Laravel, MySql e quaisquer outras tecnologias que julgar benéficas ao projeto. 
A aplicação deve prover um sistema de Login e nível de acesso simples. 
O administrador do sistema deverá manter usuários e suas permissões para a execução das seguintes tarefas:

## Permissões
- Manter produtos;
- Manter categorias;
- Manter marcas.

Não necessita desenvolver os CRUDs referentes às tarefas acima. Crie apenas uma tela para cada uma das tarefas contendo apenas o título a fim de testarmos se o usuário pode acessa-las ou não;

## Escopo
- O administrador não tem acesso às tarefas relacionadas a manter produtos, categorias e marcas;
- Apenas o CRUD de usuário é necessário.

## Tipos de Usuários (grupo)
- Administrador
- Comum
